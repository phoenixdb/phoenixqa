"""
This file is part of phoenixdb.  phoenixdb is free software: you can
redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Copyright (c) 2013, PhoenixDB Team
"""
import time
import subprocess
import re
import sys
import traceback

import base_test
from test_db import PhoenixTestDB
from results_db import ResultsDB

iaql = "iaql"


def remove_all_arrays(host, port):
    print 'Trying to cleanup database...'
    q = subprocess.Popen([iaql, "-c", host,
                          "-p", str(port),
                          "-o", "csv",
                          "-aq", "list('arrays')",
                          ],
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    out, err = q.communicate()
    if q.returncode != 0:
        print(err)
        return False

    for line in out.splitlines():
        m = re.search("'(\w+)'", line)
        if m:
            print "removing array: " + m.group(1)
            q = subprocess.Popen([iaql, "-c", host,
                                  "-p", str(port),
                                  "-aq", "remove(" + m.group(1) + ")", ],
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
            q.communicate()
            
            if q.returncode != 0:
                return False

    return True

class TestsRunner(object):
    def __init__(self, rootdir, exclude, include, record, test_env, results):
        self.rootdir = rootdir
        self.exclude = exclude
        self.include = include
        self.record = record
        self.current_test = ''
        self.error = None
        self.test_env = test_env
        self.outputdb = ResultsDB()
        self.results = results

        print("Loading tests database...")
        self._db = PhoenixTestDB(self.rootdir, self.test_env)
        print("%s tests loaded total" % self._db.count())

        if exclude or include:
            print("Setting up exclusions and inclusions...")
            self._db.set_filter(exclude=exclude, include=include)
            print("%s tests will be executed" % self._db.count())

    def run(self):
        # noinspection PyBroadException
        try:
            failed = 0
            passed = 0
            c = self._db.count()
            i = 1
            suite_time_start = time.time()
            for test in self._db.iterate_tests():
                print '%s of %s %s ...' % (i, c, test.id),
                sys.stdout.flush()
                test_time_start = time.time()
                rc = test.execute(self.outputdb, self.record)
                test_time_end = time.time()
                comment = ''
                if rc in (base_test.TEST_ERROR, base_test.FAIL):
                    comment = test.get_log()
                elif rc == base_test.DIFF:
                    comment = test.get_diff()
                self.outputdb.add_result(test.id, rc, comment, test_time_end - test_time_start)
                if rc in (base_test.PASS, base_test.RECORDED):
                    passed += 1
                if rc == base_test.PASS:
                    print "PASS"
                elif rc == base_test.RECORDED:
                    print "RECORDED"
                elif rc in (base_test.FAIL, base_test.DIFF, base_test.TEST_ERROR):
                    failed += 1
                    if rc == base_test.TEST_ERROR:
                        print "TEST_ERROR"
                    elif rc == base_test.FAIL:
                        print "FAIL"
                    elif rc == base_test.DIFF:
                        print "DIFF"
                sys.stdout.flush()

                if rc not in (base_test.PASS, base_test.RECORDED):
                    tries = 5
                    while tries:
                        if remove_all_arrays(self.test_env['host'], self.test_env['port']):
                            break
                        else:
                            tries -= 1
                            time.sleep(1)
                i += 1
            suite_time_end = time.time()
            print "Testcases executed = %s" % (passed + failed)
            print "Testcases passed = %s" % passed
            print "Testcases failed = %s" % failed

            with open(self.results, 'w') as f:
                f.write(self.outputdb.as_junit(suite_time_end - suite_time_start))

        except:
            print '\nTest runner crashed'
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = '\n'.join(traceback.format_exception(exc_type, exc_value, exc_traceback))
            print error
            exit(1)
