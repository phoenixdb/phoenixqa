"""
This file is part of phoenixdb.  phoenixdb is free software: you can
redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Copyright (c) 2013, PhoenixDB Team
"""
import os
import fnmatch

from phoenixqa.harness_test import HarnessTest


class PhoenixTestDB(object):
    def __init__(self, db_dir, test_env):
        self._db_dir = os.path.normpath(db_dir)
        self._origin_root = PhoenixTestSuite('.', '.')
        self._current_root = self._origin_root
        self._env = test_env
        self._populate_suites(self._db_dir, self._origin_root)

    def get_suites(self, exclude=None, include=None):
        if exclude or include:
            new_root = PhoenixTestSuite('.', '.')
            self._filter_suites(new_root, self._origin_root, exclude, include)
            return new_root
        else:
            return self._origin_root

    def set_filter(self, exclude=None, include=None):
        self._current_root = self.get_suites(exclude, include)

    def reset_filter(self):
        self._current_root = self._origin_root

    def iterate_tests(self):
        stack = [[self._current_root, 0]]
        while len(stack):
            node = stack[-1]
            (suite, next_child_no) = node
            if next_child_no == 0:
                for t in sorted(suite.get_tests(), key=lambda i: i.id):
                    yield t
            if len(suite.get_suites()) <= next_child_no:
                stack.pop()
            else:
                node[1] = next_child_no + 1
                next_child = sorted(suite.get_suites().values(), key=lambda s: s.id)[next_child_no]
                stack.append([next_child, 0])

    def _populate_suites(self, directory, node):
        for f in os.listdir(directory):
            p = os.path.join(directory, f)
            if os.path.isdir(p):
                suite_rel_dir = p.replace(self._db_dir + '/', '')
                suite_name = os.path.split(suite_rel_dir)[-1]
                suite_id = suite_rel_dir.replace(os.sep, '.')
                new_suite = PhoenixTestSuite(suite_id, suite_name)
                self._populate_suites(p, new_suite)
                node.add_suite(new_suite)
            elif fnmatch.fnmatch(f, '*.test'):
                test_rel_path = p.replace(self._db_dir + '/', '')
                test_id = os.path.splitext(test_rel_path)[0].replace(os.sep, '.')
                new_test = HarnessTest(p, test_id, self._env)
                node.add_test(new_test)

    @classmethod
    def _id_in_id(cls, id_a, id_b):
        id_a = id_a.split('.')
        id_b = id_b.split('.')
        if len(id_a) == len(id_b):
            return id_a == id_b
        elif len(id_a) > len(id_b):
            return id_a[0:len(id_b)] == id_b
        elif len(id_a) < len(id_b):
            return id_b[0:len(id_a)] == id_a

    @classmethod
    def _id_in_ids(cls, id_a, id_b):
        for id in id_b:
            if cls._id_in_id(id_a, id):
                return True
        return False

    @classmethod
    def _filter_suites(cls, new_node, old_node, exclude, include):
        for key, value in old_node._suites.items():
            if include and not cls._id_in_ids(value.id, include):
                continue
            if exclude and value.id in exclude:
                continue
            new_suite = PhoenixTestSuite(value.id, value.name)
            cls._filter_suites(new_suite, value, exclude, include)
            new_node.add_suite(new_suite)
        for test in old_node.get_tests():
            if include and not cls._id_in_ids(test.id, include):
                continue
            if exclude and test.id in exclude:
                continue
            new_node.add_test(test)

    def count(self):
        return self._current_root.count()


class PhoenixTestSuite(object):
    def __init__(self, id, name):
        self._id = id
        self._name = name
        self._suites = {}
        self._tests = []

    def add_suite(self, suite):
        self._suites[suite.name] = suite

    def add_test(self, test):
        self._tests.append(test)

    @property
    def id(self):
        return self._id

    @property
    def name(self):
        return self._name

    def get_tests(self):
        return self._tests

    def get_suites(self):
        return self._suites

    def count(self):
        count = len(self._tests)
        for s in self._suites.values():
            count += s.count()
        return count
