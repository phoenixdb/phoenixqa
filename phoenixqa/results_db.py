"""
This file is part of phoenixdb.  phoenixdb is free software: you can
redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Copyright (c) 2013, PhoenixDB Team
"""
from base_test import *


class ResultsDB(object):
    def __init__(self):
        self._results = []
        self._tests = 0
        self._errors = 0
        self._failures = 0
        self._skip = 0

    def add_result(self, test_id, status, comment, time):
        self._tests += 1
        if status in (FAIL, DIFF):
            self._failures += 1
        elif status == TEST_ERROR:
            self._errors += 1
        elif status == SKIP:
            self._skip += 1
        self._results.append(
            {
                'test_id': test_id,
                'status': status,
                'comment': comment,
                'time': time
            }
        )

    def as_junit(self, time):
        xml = '<?xml version="1.0" encoding="UTF-8"?>'
        xml += '<testsuite name="phoenixqa" tests="%d" errors="%d" failures="%d" skip="%d" time="%.2f">\n' % (
            self._tests, self._errors, self._failures, self._skip, time)
        for res in self._results:
            xml += '<testcase classname="Test" name="%s" time="%.2f">' % (res['test_id'], res['time'])
            status = res['status']
            if status == FAIL:
                xml += '<failure type="fail" message="Failure during test running"><![CDATA[%s]]></failure>' % res[
                    'comment']
            elif status == DIFF:
                xml += '<failure type="diff" message="Result differs from expected"><![CDATA[%s]]></failure>' % res[
                    'comment']
            elif status == TEST_ERROR:
                xml += '<error type="TestError" message="Unable complete test due error"><![CDATA[%s]]></error>' % res[
                    'comment']
            elif status == SKIP:
                xml += '<skipped/>'
            xml += '</testcase>\n'
        xml += '</testsuite>\n'
        return xml
