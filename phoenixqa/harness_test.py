"""
This file is part of phoenixdb.  phoenixdb is free software: you can
redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Copyright (c) 2013, PhoenixDB Team
"""
import os
import sys
import difflib
import subprocess
import time
import re
from StringIO import StringIO
from contextlib import closing
import traceback

from phoenixqa import base_test


class HarnessTest(base_test.BaseTest):
    def __init__(self, test_file, test_id, test_env):
        self.test_file = test_file
        self.test_id = test_id
        self.format = "dcsv"
        self.igdata = ""
        self.logging = False
        self.test_env = dict(test_env)
        self.test_env['TESTDIR'] = os.path.dirname(self.test_file)
        self.host = test_env.get('host', 'localhost')
        self.port = test_env.get('port', 1239)
        self._diff = None
        self._log = None
        self._precision = 6

    @property
    def id(self):
        return self.test_id

    def get_diff(self):
        return self._diff

    def get_log(self):
        return self._log

    def execute(self, resultdb, record=False):
        rc = base_test.PASS
        diff = self.test_file.replace('.test', '.diff')
        output = self.test_file.replace('.test', '.out')
        expected = self.test_file.replace('.test', '.expected')
        log = self.test_file.replace('.test', '.log')

        with closing(StringIO()) as fl, closing(StringIO()) as fr:
            try:
                if os.path.exists(diff):
                    os.remove(diff)
                if os.path.exists(output):
                    os.remove(output)
                if os.path.exists(log):
                    os.remove(log)

                fl.write('Test environment:\n')
                fl.write('\n'.join(['%s=%s' % (key, value) for (key, value) in self.test_env.items()]))
                fl.write('\n')

                with open(self.test_file, 'r') as ft:
                    for line in ft:
                        rc = self._execute_line(line, fr, fl)
                        if rc != base_test.PASS:
                            break

                if rc == base_test.PASS:
                    if record:
                        with open(expected, 'w') as result_file:
                            result_file.write(fr.getvalue())
                        return base_test.RECORDED

                    with open(expected, 'r') as fd:
                        fr.seek(0)
                        diff_content = list(difflib.unified_diff(fd.readlines(), fr.readlines(), self.id + ' expected',
                                                                 self.id + ' output'))

                    if len(diff_content):
                        with open(diff, 'w') as fd:
                            fd.writelines(diff_content)
                            rc = base_test.DIFF
                            self._diff = ''.join(diff_content)

                sys.stdout.flush()
            except Exception as e:
                fl.write('Caught exception during testing: %s\n' % str(e))
                fl.write('Traceback:\n')
                traceback.print_exc(file=fl)
                rc = base_test.TEST_ERROR
            finally:
                if rc not in (base_test.PASS, base_test.RECORDED):
                    with open(log, 'w') as fd:
                        fd.writelines(fl.getvalue())
                    with open(output, 'w') as fd:
                        fd.writelines(fr.getvalue())
                    self._log = fl.getvalue()
        return rc

    def _subst_vars(self, string):
        res = string
        for var in re.findall('\${(\w+)}', string):
            if var in self.test_env:
                res = re.sub('\$\{%s\}' % var, self.test_env[var], res)
        return res

    def _execute_line(self, line, ofile, logfile):
        logfile.write("Test line: %s\n" % line)
        afl = "-a"
        error = ""
        igdata = self.igdata
        justrun = False

        line = line.strip()
        if (line == "" or
                    line == "--setup" or
                    line == "--test" or
                    line == "--cleanup" or
                    line.find("--start-timer") != -1 or
                    line == "--stop-timer" or
                    line[0] == "#"):
            return base_test.PASS

        if line == "--start-query-logging":
            self.logging = True
            logfile.write("Start query logging\n")
            return base_test.PASS
        if line == "--stop-query-logging":
            self.logging = False
            logfile.write("Stop query logging\n")
            return base_test.PASS

        if line.find("--set-precision") != -1:
            line = line.replace("--set-precision", "")
            line = line.strip(" ")
            self._precision = int(line)
            return base_test.PASS

        #FIXME: Do it even needed?
        if line == "--start-ignore-warnings":
            return base_test.PASS
        if line == "--stop-ignore-warnings":
            return base_test.PASS

        if line.find("--shell") != -1:
            line = line.replace("--shell", "")
            line = line.strip(" ")
            out = subprocess.PIPE
            logfile.write("Will execute shell command\n")
            if line.find("--store") != -1:
                line = line.replace("--store", "")
                line = line.strip(" ")
                out = ofile
                logfile.write("Store result to output\n")
            if line.find("--command") != -1:
                line = line.replace("--command", "")
                line = line.strip(" ")
                line = line.strip("=")
                line = line.strip("\"")
            if self.logging and out == ofile:
                ofile.write("SCIDB QUERY : <" + line + ">\n")
                ofile.flush()
                logfile.write(line + "\n")
            line = self._subst_vars(line)
            q = subprocess.Popen(line, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True,
                                 universal_newlines=True, executable='/bin/bash', env=dict(os.environ, **self.test_env))
            (stdout, stderr) = q.communicate()
            rc = q.returncode
            if out != subprocess.PIPE:
                out.write(stdout)
            logfile.write(stderr)
            if rc == 0:
                logfile.write("Shell command executed\n")
            else:
                logfile.write("Shell command failed\n")
                logfile.flush()
                return base_test.FAIL
            if out == ofile:
                ofile.write("\n")
                ofile.flush()
            return base_test.PASS

        if line == "--start-igdata":
            self.igdata = "-n"
            logfile.write("Start data ignoring\n")
            return base_test.PASS
        if line == "--stop-igdata":
            self.igdata = ""
            logfile.write("Stop data ignoring\n")
            return base_test.PASS

        if line.find("--echo") != -1:
            line = line.replace("--echo", "")
            line = line.strip(" ")
            ofile.write(line + "\n")
            line = self._subst_vars(line)
            logfile.write("Echo: %s\n" % line)
            return base_test.PASS

        if line.find("--sleep") != -1:
            line = line.replace("--sleep", "")
            line = line.strip("= \"")
            logfile.write("Start sleep: %s\n" % line)
            line = self._subst_vars(line)
            time.sleep(int(line))
            logfile.write("Done sleep\n")
            return base_test.PASS

        if line.find("--reset-format") != -1:
            self.format = "dcsv"
            logfile.write("Reset format\n")
            return base_test.PASS
        if line.find("--set-format") != -1:
            line = line.replace("--set-format ", "")
            line = line.strip()
            line = self._subst_vars(line)
            self.format = line
            logfile.write("Set format to %s\n" % line)
            return base_test.PASS

        if line.find("--igdata") != -1:
            line = line.replace("--igdata", "")
            line = line.strip(' \"')
            igdata = "-n"
            logfile.write("Will ignore data")

        if line.find("--error") != -1:
            line = line.replace("--error", "")
            line = line.strip()
            logfile.write("Negative tests detected\n")
            if line.find("--code") != -1:
                line = line.replace("--code", "")
                line = line.strip(" =")
                error = line.split()[0]
                line = line.replace(error, "")
                line = line.strip()
                logfile.write("Expected error code: %s\n" % error)
                if line.find("--aql") != -1:
                    line = line.replace("--aql", "")
                    line = line.strip("= \"")
                    afl = ""
                    logfile.write("Query is AQL\n")
                else:
                    line = line.strip(" \"")
            else:
                line = line.strip(" \"")
                error = "any"
                logfile.write("Expected error code: %s\n" % error)

        if line.find("--aql") != -1:
            line = line.replace("--aql", "")
            line = line.strip("= \"")
            afl = ""
            logfile.write("Query is AQL\n")

        if line.find("--afl") != -1:
            line = line.replace("--afl", "")
            line = line.strip("= \"")
            logfile.write("Query is AFL\n")

        if line.find("--justrun") != -1:
            line = line.replace("--justrun", "")
            line = line.strip(' \"')
            justrun = True
            logfile.write("Just run\n")

        c = line.find('#')
        if c != -1:
            line = line[0:c]
            print "line without comment: " + line

        if line.find("--") != -1:
            print >> sys.stderr, line
            logfile.write("Unknown command: %s\n" % line)
            return base_test.TEST_ERROR

        if self.logging:
            ofile.write("SCIDB QUERY : <" + line + ">\n")
        if igdata != "":
            ofile.write("[")
        ofile.flush()
        logfile.write("Executing query\n")

        subst_line = self._subst_vars(line)
        q = subprocess.Popen(["iaql", "-c", self.host, "-p", str(self.port),
                              "-o", self.format, igdata, afl, "-w", str(self._precision), "-q", subst_line],
                             #stdout=ofile,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE, universal_newlines=True)
        (stdout, stderr) = q.communicate()
        rc = q.returncode
        ofile.write(stderr)
        ofile.write(stdout)
        if rc == 0:
            logfile.write("Executed successfully\n")
            # noinspection PyBroadException
            try:
                s = -2 if os.name == "nt" else -1
                ofile.seek(s, 1)
            except:
                pass
            rc = base_test.PASS
        else:
            logfile.write("Fail during query execution: %s" % stderr)
            if not justrun:
                error2 = stderr.split('\n')[1].split()[2]
                if error != "any":
                    ofile.write("[An error expected at this place for the query \"" + line +
                                "\". And it failed with error code = " + error2 +
                                ". Expected error code = " + error + ".]")
                    if error2 != error:
                        logfile.write("Unexpected error code: %s\n" % error2)
                        rc = base_test.FAIL
                    else:
                        logfile.write("Expected error during query execution\n")
                        rc = base_test.PASS
                else:
                    ofile.write("[An error expected at this place for the query \"" + line +
                                "\". And it failed.]")
                    rc = base_test.PASS
            else:
                ofile.write("[SciDB query execution failed. But continuing, as it was intended to just run.]")
                logfile.write("Expected error during query execution\n")
                rc = base_test.PASS

        if igdata != "":
            ofile.write(", ignoring data output by this query.]")
        ofile.write("\n\n")
        ofile.flush()
        logfile.flush()
        return rc
