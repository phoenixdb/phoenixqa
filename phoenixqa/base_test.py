"""
This file is part of phoenixdb.  phoenixdb is free software: you can
redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Copyright (c) 2013, PhoenixDB Team
"""
SKIP = -2
RECORDED = -1
PASS = 0
FAIL = 2
DIFF = 3
TEST_ERROR = 4


class BaseTest(object):
    @property
    def id(self):
        raise NotImplementedError

    def execute(self, resultdb, record=False):
        raise NotImplementedError

    def get_diff(self):
        raise NotImplementedError

    def get_log(self):
        raise NotImplementedError
