#!/usr/bin/python
"""
This file is part of phoenixdb.  phoenixdb is free software: you can
redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Copyright (c) 2013, PhoenixDB Team
"""
import argparse
import os
import re
import sys
import phoenixqa
import phoenixqa.test_runner


def which(program):
    import os

    def is_exe(file_path):
        return os.path.isfile(file_path) and os.access(file_path, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--root-dir', dest='rootdir', help='Root test directory')
    parser.add_argument('-o', '--results', dest='results', default="./results.xml", help='Results in JUnit-like format')
    parser.add_argument('-C', '--cleanup', dest='cleanup', action='store_true',
                        help='Remove all arrays before executing tests')
    parser.add_argument('-P', '--prefix', dest='prefix', help='PhoenixDB prefix to setup PATH')
    parser.add_argument('-D', '--define', dest='env', action='append',
                        help='Set test environment variable (format variable=value)')
    parser.add_argument('-E', '--environment', dest='env_file', type=str,
                        help=
                        'Test environment file in Python dictionary format.\n'
                        'System environment variables: host port prefix')

    parser.add_argument('-R', '--record', dest='record', action='store_true', help='record test result')
    parser.add_argument('-e', '--exclude', dest='exclude', default=None, nargs='+',
                        help='Tests and suites list ids to exclude from execution')
    parser.add_argument(dest='include', metavar='id', default=None, nargs='*',
                        help='Tests and suites ids list to run')

    pargs = parser.parse_args()
    if pargs.env_file:
        with open(pargs.env_file) as f:
            try:
                test_env = eval(f.read())
            except:
                print 'Unable to evaluate file %s. File should be plain python dictionary:' % pargs.env_file
                raise
    else:
        test_env = {}

    if pargs.env:
        for var in pargs.env:
            g = re.match('([a-zA-Z]+\w*)\s*=\s*(.*)', var)
            if not g:
                print "Can not parse '%s'. Format must be 'variable=value' " % var
                exit(1)
            test_env[g.groups()[0]] = g.groups()[1]

    if 'prefix' in test_env:
        os.environ['PATH'] = \
            os.path.join(test_env['prefix'], 'bin') + os.pathsep + \
            os.path.join(test_env['prefix'], 'lib', 'phoenixdb', 'bin') + os.pathsep + \
            os.environ['PATH']

    if 'utils_dir' in test_env:
        os.environ['PATH'] = os.path.join(test_env['utils_dir']) + os.pathsep + os.environ['PATH']

    if 'path' in test_env:
        os.environ['PATH'] = test_env['path'] + os.pathsep + os.environ['PATH']

    if 'rootdir' in test_env and not pargs.rootdir:
        pargs.rootdir = test_env['rootdir']

    if not which("iaql"):
        print 'Can not find iaql binary. Set \'prefix\' variable with path to PhoenixDB distributive'
        exit(1)

    if not os.path.exists(pargs.rootdir):
        print "Cannot find tests dir '%s'" % pargs.rootdir
        sys.exit(1)

    if 'host' not in test_env:
        test_env['host'] = 'localhost'

    if 'port' not in test_env:
        test_env['port'] = '1239'

    if pargs.cleanup:
        if not phoenixqa.test_runner.remove_all_arrays(test_env['host'], test_env['port']):
            print('Unable to remove arrays')
            exit(1)

    exclude = []
    if pargs.exclude:
        for ex in pargs.exclude:
            if not os.path.exists(ex):
                exclude.append(ex)
                continue

            with open(ex) as f:
                for l in f.readlines():
                    l = l.strip()
                    if l and l[0] != '#':
                        exclude.append(l)

    test_env['HPID'] = str(os.getpid())
    runner = phoenixqa.test_runner.TestsRunner(pargs.rootdir, exclude, pargs.include,
                                               pargs.record, test_env, pargs.results)
    runner.run()


if __name__ == "__main__":
    main()
